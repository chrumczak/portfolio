import React from 'react'
import styled from 'styled-components'

const Root = styled.button`
    box-shadow: none;
`

const Button = ({ children, ...props }) => {
    return (
        <Root {...props}>
            {children}
        </Root>
    )
}

export default Button
